package Runner;

import java.io.File;
import java.lang.reflect.Method;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.alti.reporter.ExtentManager;
import com.alti.reporter.ExtentTestManager;
import com.cucumber.listener.ExtentCucumberFormatter;
import com.relevantcodes.extentreports.LogStatus;

import cucumber.api.CucumberOptions;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features= {"/src/test/resources/Features"},
glue= {"StepDefs"},
monochrome=true,
plugin = {"pretty","html:target/cucumber","com.cucumber.listener.ExtentCucumberFormatter"}
)
public class CukeRunner extends AbstractTestNGCucumberTests{
	
	@BeforeClass
	public static void setup() {
		ExtentCucumberFormatter.initiateExtentCucumberFormatter();
		ExtentCucumberFormatter.loadConfig(new File("src/test/resources/Configurations/extent-config.xml"));
		ExtentCucumberFormatter.addSystemInfo("Browser Name","Chrome");
		ExtentCucumberFormatter.addSystemInfo("Browser Version","Version 77");
		ExtentCucumberFormatter.addSystemInfo("Selenium Version","Ver2.53.1");
		
		
	}
	
	

}
