package runner;

import java.io.File;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.alti.base.Driverutils;
import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;


@CucumberOptions(
features= {"src/test/java/features"},
glue= {"stepsDefinitions"},
monochrome=true,
plugin = {"pretty","html:Reports/cucumber-Pretty",
		"json:Reports/cucumber-json/cucumber.json",
		"com.cucumber.listener.ExtentCucumberFormatter:Reports/html/extentReport.html"}
)

@Test
public class TestRunner extends AbstractTestNGCucumberTests{
	
	@AfterClass
	public static void reportSetup() {
		Reporter.loadXMLConfig(new File("src/test/resources/Configurations/extent-config.xml"));
		Reporter.setSystemInfo("User Name", System.getProperty("user.name"));
		Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
		Reporter.setSystemInfo("64 Bit", "Windows 10");
		Reporter.setSystemInfo("3.10", "selenium");
		Reporter.setTestRunnerOutput("Ticket Master Disovery Test output");
		
	}
	
	@AfterMethod(alwaysRun=true)
		public void cleanup() {
		Driverutils.tearDown();
	}
	

}
	
	
	
	


